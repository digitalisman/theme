<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "Cs.Theme".
 *
 * Auto generated 01-06-2018 22:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = [
    'title' => 'Fluid Typo3 Theme',
    'description' => 'Digitalisman Theme by Daniel Vogel',
    'category' => 'misc',
    'shy' => 0,
    'version' => '1.1.0',
    'dependencies' => 'typo3',
    'conflicts' => '',
    'priority' => '',
    'loadOrder' => '',
    'module' => '',
    'state' => 'stable',
    'uploadfolder' => 0,
    'createDirs' => 'fileadmin/user_upload/call_to_action','fileadmin/user_upload/News','fileadmin/user_upload/Inhalt','fileadmin/user_upload/Sonstiges',
    'modify_tables' => '',
    'clearCacheOnLoad' => 1,
    'lockType' => '',
    'author' => 'Daniel Vogel',
    'author_email' => 'info@digitalisman.de',
    'author_company' => 'Digitalisman',
    'CGLcompliance' => '',
    'CGLcompliance_note' => '',
    'constraints' => [
        'depends' => [
            'typo3' => '8.7.0-9.9.99',
            'fluid_styled_content' => '',
            'rte_ckeditor' => '',
            'extbase' => '',
            'fluid' => '',
            'scheduler' => '',
            //'ws_scss' => '',
            //'vhs_scss' => '',
            'vhs' => '',
            'mask' => '',
            'imageoptimizer' => ''
        ],
        'conflicts' => [
        ],
        'suggests' => [
        ],
    ],
    'autoload' =>
    [
        'psr-4' =>
            [
                "CS\\Theme\\" => "Classes/"
            ]
    ],
    '_md5_values_when_last_written' => 'a:0:{}',
    'suggests' => [],
];
