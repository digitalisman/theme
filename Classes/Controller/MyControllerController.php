<?php
class Tx_Rsysworkbook_Controller_ExportController extends Tx_Extbase_MVC_Controller_ActionController {

    // ------------------------------------------------------------
    // properties
    // ------------------------------------------------------------
   
    protected $settings = array();
   
    /**
     * frontendUserRepository
     *
     * @var Tx_Rsysworkbook_Domain_Repository_FrontendUserRepository
     */
    protected $frontendUserRepository;
   
    protected function initializeAction() {
        $this->frontendUserRepository = t3lib_div::makeInstance('Tx_Rsysworkbook_Domain_Repository_FrontendUserRepository');
       
        $configurationManager = t3lib_div::makeInstance('Tx_Extbase_Configuration_BackendConfigurationManager');
        $this->settings = $configurationManager->getConfiguration(
                $this->request->getControllerExtensionName(),
                $this->request->getPluginName()
        );
        $this->settings = $this->settings['settings'];
    }
   
   

    /**
     * action list
     *
     * @return void
     */
    public function indexAction() {
        // just renders template
    }

   
    /**
     * action export
     *
     * @return void
     */
    public function exportAction() {

        // do all the export stuff here...

        // ----------------------------------------
        // download csv file
        // ----------------------------------------
        // prepare infos
        $cType = 'application/csv';
        $fileLen = filesize($exportFileName);
        $headers = array(
                'Pragma' => 'public',
                'Expires' => 0,
                'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
                'Cache-Control' => 'public',
                'Content-Description' => 'File Transfer',
                'Content-Type' => $cType,
                'Content-Disposition' => 'attachment; filename="'.$fname.'"',
                'Content-Transfer-Encoding' => 'binary',
                'Content-Length' => $fileLen
        );
   
        // send headers
        foreach ($headers as $header => $data)
            $this->response->setHeader($header, $data);
   
        $this->response->sendHeaders();
   
        // send file
        @readfile($exportFileName);
        exit;
       
    }
       
}
?>