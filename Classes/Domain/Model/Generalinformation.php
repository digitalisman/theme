<?php

namespace CS\Theme\Domain\Model;

/***
 *
 * This file is part of the "Generalinformation" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sven Harders <typo3@svenharders.de>, s;harders
 *
 ***/

/**
 * Generalinformation
 */
class Generalinformation extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * companyname
     *
     * @var string
     */
    protected $companyname = '';

    /**
     * logo
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $logo = null;

    /**
     * address
     *
     * @var string
     */
    protected $address = '';

    /**
     * zip
     *
     * @var string
     */
    protected $zip = '';

    /**
     * city
     *
     * @var string
     */
    protected $city = '';

    /**
     * email
     *
     * @var string
     */
    protected $email = '';

    /**
     * phone
     *
     * @var string
     */
    protected $phone = '';

    /**
     * opentimes
     *
     * @var string
     */
    protected $opentimes = '';

    /**
     * fax
     *
     * @var string
     */
    protected $fax = '';
    
    /**
     * registergericht
     *
     * @var string
     */
    protected $registrationcourt = '';
    
    /**
     * registernummer
     *
     * @var string
     */
    protected $registrationnumber = '';
    
    /**
     * CEO
     *
     * @var string
     */
    protected $ceo = '';
	
	/**
     * Umsatzsteuer ID
     *
     * @var string
     */
    protected $taxid = '';

    /**
     * fields
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $fields = null;

    /**
     * socialmedias
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $socialmedias = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->fields = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->socialmedias = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the companyname
     *
     * @return string $companyname
     */
    public function getCompanyname()
    {
        return $this->companyname;
    }

    /**
     * Sets the companyname
     *
     * @param string $companyname
     * @return void
     */
    public function setCompanyname($companyname)
    {
        $this->companyname = $companyname;
    }

    /**
     * Returns the logo
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Sets the logo
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
     * @return void
     */
    public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
    {
        $this->logo = $logo;
    }

    /**
     * Returns the address
     *
     * @return string $address
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Sets the address
     *
     * @param string $address
     * @return void
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * Returns the zip
     *
     * @return string $zip
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Sets the zip
     *
     * @param string $zip
     * @return void
     */
    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    /**
     * Returns the city
     *
     * @return string $city
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Sets the city
     *
     * @param string $city
     * @return void
     */
    public function setCity($city)
    {
        $this->city = $city;
    }

    /**
     * Returns the email
     *
     * @return string $email
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Sets the email
     *
     * @param string $email
     * @return void
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * Returns the phone
     *
     * @return string $phone
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Sets the phone
     *
     * @param string $phone
     * @return void
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Returns the opentimes
     *
     * @return string $opentimes
     */
    public function getOpentimes()
    {
        return $this->opentimes;
    }

    /**
     * Sets the opentimes
     *
     * @param string $opentimes
     * @return void
     */
    public function setOpentimes($opentimes)
    {
        $this->opentimes = $opentimes;
    }

    /**
     * Returns the fax
     *
     * @return string $fax
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Sets the fax
     *
     * @param string $fax
     * @return void
     */
    public function setFax($fax)
    {
        $this->fax = $fax;
    }
    
    /**
	 * Returns the registrationcourt
     *
     * @return string $registrationcourt
     */
    public function getRegistrationcourt()
    {
        return $this->registrationcourt;
    }

    /**
     * Sets the registrationcourt
     *
     * @param string $registrationcourt
     * @return void
     */
    public function setRegistrationcourt($registrationcourt)
    {
        $this->registrationcourt = $registrationcourt;
	}
	
	/**
	 * Returns the registrationnumber
     *
     * @return string $registrationnumber
     */
    public function getRegistrationnumber()
    {
        return $this->registrationnumber;
    }

    /**
     * Sets the registrationnumber
     *
     * @param string $registrationnumber
     * @return void
     */
    public function setRegistrationnumber($registrationnumber)
    {
        $this->registrationnumber = $registrationnumber;
    }
    
    /**
	 * Returns the ceo
     *
     * @return string $ceo
     */
    public function getCeo()
    {
        return $this->ceo;
    }

    /**
     * Sets the ceo
     *
     * @param string $ceo
     * @return void
     */
    public function setCeo($ceo)
    {
        $this->ceo = $ceo;
    }
    
    /**
	 * Returns the taxid
     *
     * @return string $taxid
     */
    public function getTaxid()
    {
        return $this->taxid;
    }

    /**
     * Sets the taxid
     *
     * @param string $taxid
     * @return void
     */
    public function setTaxid($taxid)
    {
        $this->taxid = $taxid;
    }

    /**
     * Adds a Field
     *
     * @param \CS\Theme\Domain\Model\Field $field
     * @return void
     */
    public function addField(\CS\Theme\Domain\Model\Field $field)
    {
        $this->fields->attach($field);
    }

    /**
     * Removes a Field
     *
     * @param \CS\Theme\Domain\Model\Field $fieldToRemove The Field to be removed
     * @return void
     */
    public function removeField(\CS\Theme\Domain\Model\Field $fieldToRemove)
    {
        $this->fields->detach($fieldToRemove);
    }

    /**
     * Returns the fields
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field> $fields
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Sets the fields
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Field> $fields
     * @return void
     */
    public function setFields(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $fields)
    {
        $this->fields = $fields;
    }

    /**
     * Adds a Socialmedia
     *
     * @param \CS\Theme\Domain\Model\Socialmedia $socialmedia
     * @return void
     */
    public function addSocialmedia(\CS\Theme\Domain\Model\Socialmedia $socialmedia)
    {
        $this->socialmedias->attach($socialmedia);
    }

    /**
     * Removes a Socialmedia
     *
     * @param \CS\Theme\Domain\Model\Socialmedia $socialmediaToRemove The Socialmedia to be removed
     * @return void
     */
    public function removeSocialmedia(\CS\Theme\Domain\Model\Socialmedia $socialmediaToRemove)
    {
        $this->socialmedias->detach($socialmediaToRemove);
    }

    /**
     * Returns the socialmedias
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia> $socialmedias
     */
    public function getSocialmedias()
    {
        return $this->socialmedias;
    }

    /**
     * Sets the socialmedias
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\CS\Theme\Domain\Model\Socialmedia> $socialmedias
     * @return void
     */
    public function setSocialmedias(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $socialmedias)
    {
        $this->socialmedias = $socialmedias;
    }
}
