<?php
namespace CS\Theme\Domain\Model;

/***
 *
 * This file is part of the "Generalinformation" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sven Harders <typo3@svenharders.de>, s;harders
 *
 ***/

/**
 * Field
 */
class Field extends \TYPO3\CMS\Extbase\DomainObject\AbstractValueObject
{
    /**
     * type
     *
     * @var int
     */
    protected $type = 0;

    /**
     * title
     *
     * @var string
     */
    protected $title = '';

    /**
     * variablename
     *
     * @var string
     */
    protected $variablename = '';

    /**
     * value
     *
     * @var string
     */
    protected $value = '';

    /**
     * image
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade remove
     */
    protected $image = null;

    /**
     * Returns the type
     *
     * @return int $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param int $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the variablename
     *
     * @return string $variablename
     */
    public function getVariablename()
    {
        return $this->variablename;
    }

    /**
     * Sets the variablename
     *
     * @param string $variablename
     * @return void
     */
    public function setVariablename($variablename)
    {
        $this->variablename = $variablename;
    }

    /**
     * Returns the value
     *
     * @return string $value
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value
     *
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Returns the image
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Sets the image
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $image
     * @return void
     */
    public function setImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $image)
    {
        $this->image = $image;
    }
}
