<?php

namespace CS\Theme\Domain\Repository;

/***
 *
 * This file is part of the "Generalinformation" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018 Sven Harders <typo3@svenharders.de>, s;harders
 *
 ***/

/**
 * The repository for Generalinformations
 */
class GeneralinformationRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
}
