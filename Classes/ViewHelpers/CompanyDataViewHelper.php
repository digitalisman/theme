<?php

namespace CS\Theme\ViewHelpers;

/**
 * This file is part of the "Theme" Extension for TYPO3 CMS.
 *  (c) 2019 Digitalisman - Daniel Vogel <info@digitalisman.de>
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * ViewHelper to create a valid JsonObject from a bunch of addresses
 */
class CompanyDataViewHelper extends AbstractViewHelper
{
    protected $escapeOutput = false;
    
    /**
     * Arguments initialization
     */
    public function initializeArguments()
    {
        // only if needed :)
        //$this->registerArgument('settings', 'array', 'Settings Array', 1);
    }

    /**
     * Renders Variables to the template
     *
     * @return void
     */
    public function render()
    {
        $objectManager = GeneralUtility::makeInstance('TYPO3\\CMS\\Extbase\\Object\\ObjectManager');

        /** @var ConfigurationManagerInterface $configurationManager */
        $configurationManager = $objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface');
        $settings = $configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT,
            'theme',
            'theme_pi1'
        );

        $companyDataUid = $settings['plugin.']['tx_theme.']['settings.']['companyDataUid'];

        if ($companyDataUid) {
            $repository = $objectManager->get('CS\\Theme\\Domain\\Repository\\GeneralInformationRepository');
            $this->templateVariableContainer->add('companyData', $repository->findByUid($companyDataUid));
        } else {
            $this->templateVariableContainer->add('companyData', []);
        }
    }
}
