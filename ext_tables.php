<?php
if (!defined('TYPO3_MODE')) {
	die ('Access denied.');
}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theme_domain_model_generalinformation', 'EXT:theme/Resources/Private/Language/locallang_csh_tx_theme_domain_model_generalinformation.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theme_domain_model_generalinformation');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theme_domain_model_field', 'EXT:theme/Resources/Private/Language/locallang_csh_tx_theme_domain_model_field.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theme_domain_model_field');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_theme_domain_model_socialmedia', 'EXT:theme/Resources/Private/Language/locallang_csh_tx_theme_domain_model_socialmedia.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_theme_domain_model_socialmedia');
