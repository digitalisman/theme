CREATE TABLE tt_content
(
  headertype    varchar(255) DEFAULT '' NOT NULL,
  subheadertype varchar(255) DEFAULT '' NOT NULL,
);

CREATE TABLE pages
(
  cssclass      varchar(255) DEFAULT '' NOT NULL,
  indexnofollow tinyint(4) unsigned DEFAULT '0' NOT NULL,
);


CREATE TABLE tx_theme_domain_model_generalinformation
(

  uid              int(11) NOT NULL auto_increment,
  pid              int(11) DEFAULT '0' NOT NULL,

  companyname      varchar(255) DEFAULT '' NOT NULL,
  logo             int(11) unsigned NOT NULL default '0',
  address          varchar(255) DEFAULT '' NOT NULL,
  zip              varchar(255) DEFAULT '' NOT NULL,
  city             varchar(255) DEFAULT '' NOT NULL,
  email            varchar(255) DEFAULT '' NOT NULL,
  phone            varchar(255) DEFAULT '' NOT NULL,
  opentimes        text,
  fax              varchar(255) DEFAULT '' NOT NULL,
  fields           int(11) unsigned DEFAULT '0' NOT NULL,
  socialmedias     int(11) unsigned DEFAULT '0' NOT NULL,

  tstamp           int(11) unsigned DEFAULT '0' NOT NULL,
  crdate           int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id        int(11) unsigned DEFAULT '0' NOT NULL,
  deleted          smallint(5) unsigned DEFAULT '0' NOT NULL,
  hidden           smallint(5) unsigned DEFAULT '0' NOT NULL,
  starttime        int(11) unsigned DEFAULT '0' NOT NULL,
  endtime          int(11) unsigned DEFAULT '0' NOT NULL,

  t3ver_oid        int(11) DEFAULT '0' NOT NULL,
  t3ver_id         int(11) DEFAULT '0' NOT NULL,
  t3ver_wsid       int(11) DEFAULT '0' NOT NULL,
  t3ver_label      varchar(255) DEFAULT '' NOT NULL,
  t3ver_state      smallint(6) DEFAULT '0' NOT NULL,
  t3ver_stage      int(11) DEFAULT '0' NOT NULL,
  t3ver_count      int(11) DEFAULT '0' NOT NULL,
  t3ver_tstamp     int(11) DEFAULT '0' NOT NULL,
  t3ver_move_id    int(11) DEFAULT '0' NOT NULL,

  sys_language_uid int(11) DEFAULT '0' NOT NULL,
  l10n_parent      int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource  mediumblob,
  l10n_state       text,

  PRIMARY KEY (uid),
  KEY              parent (pid),
  KEY              t3ver_oid (t3ver_oid,t3ver_wsid),
  KEY language (l10n_parent,sys_language_uid)

);

CREATE TABLE tx_theme_domain_model_field
(

  uid                int(11) NOT NULL auto_increment,
  pid                int(11) DEFAULT '0' NOT NULL,

  generalinformation int(11) unsigned DEFAULT '0' NOT NULL,

  type               int(11) DEFAULT '0' NOT NULL,
  title              varchar(255) DEFAULT '' NOT NULL,
  variablename       varchar(255) DEFAULT '' NOT NULL,
  value              varchar(255) DEFAULT '' NOT NULL,
  image              int(11) unsigned NOT NULL default '0',

  tstamp             int(11) unsigned DEFAULT '0' NOT NULL,
  crdate             int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id          int(11) unsigned DEFAULT '0' NOT NULL,
  deleted            smallint(5) unsigned DEFAULT '0' NOT NULL,
  hidden             smallint(5) unsigned DEFAULT '0' NOT NULL,
  starttime          int(11) unsigned DEFAULT '0' NOT NULL,
  endtime            int(11) unsigned DEFAULT '0' NOT NULL,

  t3ver_oid          int(11) DEFAULT '0' NOT NULL,
  t3ver_id           int(11) DEFAULT '0' NOT NULL,
  t3ver_wsid         int(11) DEFAULT '0' NOT NULL,
  t3ver_label        varchar(255) DEFAULT '' NOT NULL,
  t3ver_state        smallint(6) DEFAULT '0' NOT NULL,
  t3ver_stage        int(11) DEFAULT '0' NOT NULL,
  t3ver_count        int(11) DEFAULT '0' NOT NULL,
  t3ver_tstamp       int(11) DEFAULT '0' NOT NULL,
  t3ver_move_id      int(11) DEFAULT '0' NOT NULL,
  sorting            int(11) DEFAULT '0' NOT NULL,

  sys_language_uid   int(11) DEFAULT '0' NOT NULL,
  l10n_parent        int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource    mediumblob,
  l10n_state         text,

  PRIMARY KEY (uid),
  KEY                parent (pid),
  KEY                t3ver_oid (t3ver_oid,t3ver_wsid),
  KEY language (l10n_parent,sys_language_uid)

);


CREATE TABLE tx_theme_domain_model_socialmedia
(

  uid                int(11) NOT NULL auto_increment,
  pid                int(11) DEFAULT '0' NOT NULL,

  generalinformation int(11) unsigned DEFAULT '0' NOT NULL,

  title              varchar(255) DEFAULT '' NOT NULL,
  username           varchar(255) DEFAULT '' NOT NULL,
  profileurl         varchar(255) DEFAULT '' NOT NULL,
  profilepicture     int(11) unsigned NOT NULL default '0',

  tstamp             int(11) unsigned DEFAULT '0' NOT NULL,
  crdate             int(11) unsigned DEFAULT '0' NOT NULL,
  cruser_id          int(11) unsigned DEFAULT '0' NOT NULL,
  deleted            smallint(5) unsigned DEFAULT '0' NOT NULL,
  hidden             smallint(5) unsigned DEFAULT '0' NOT NULL,
  starttime          int(11) unsigned DEFAULT '0' NOT NULL,
  endtime            int(11) unsigned DEFAULT '0' NOT NULL,

  t3ver_oid          int(11) DEFAULT '0' NOT NULL,
  t3ver_id           int(11) DEFAULT '0' NOT NULL,
  t3ver_wsid         int(11) DEFAULT '0' NOT NULL,
  t3ver_label        varchar(255) DEFAULT '' NOT NULL,
  t3ver_state        smallint(6) DEFAULT '0' NOT NULL,
  t3ver_stage        int(11) DEFAULT '0' NOT NULL,
  t3ver_count        int(11) DEFAULT '0' NOT NULL,
  t3ver_tstamp       int(11) DEFAULT '0' NOT NULL,
  t3ver_move_id      int(11) DEFAULT '0' NOT NULL,
  sorting            int(11) DEFAULT '0' NOT NULL,

  sys_language_uid   int(11) DEFAULT '0' NOT NULL,
  l10n_parent        int(11) DEFAULT '0' NOT NULL,
  l10n_diffsource    mediumblob,
  l10n_state         text,

  PRIMARY KEY (uid),
  KEY                parent (pid),
  KEY                t3ver_oid (t3ver_oid,t3ver_wsid),
  KEY language (l10n_parent,sys_language_uid)

);


CREATE TABLE tx_theme_domain_model_field
(

  generalinformation int(11) unsigned DEFAULT '0' NOT NULL,

);


CREATE TABLE tx_theme_domain_model_socialmedia
(

  generalinformation int(11) unsigned DEFAULT '0' NOT NULL,

);
