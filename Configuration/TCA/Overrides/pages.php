<?php
if (!defined('TYPO3_MODE')) {
    die ('Access denied.');
}

$fields = [
    'cssclass' => [
        'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang.xlf:pages.cssclass',
        'exclude' => 1,
        'config' => [
            'type' => 'input',
            'max' => 255
        ],
    ],
    'indexnofollow' => [
    'exclude' => true,
    'label' => 'LLL:EXT:theme/Resources/Private/Language/locallang.xlf:pages.indexnofollow',
    'config' => [
        'type' => 'check',
        'items' => [
            '1' => [
                '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
            ]
        ],
    ],
],
];

// Add new fields to pages:
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('pages', $fields, 1);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
    'pages',
    '--palette--;LLL:EXT:theme/Resources/Private/Language/locallang.xlf:pages.palette_title;cssclass,indexnofollow',
    '',
    'after:nav_title'
);

$GLOBALS['TCA']['pages']['palettes']['cssclass'] = ['showitem' => 'cssclass'];
$GLOBALS['TCA']['pages']['palettes']['indexnofollow'] = ['showitem' => 'indexnofollow'];